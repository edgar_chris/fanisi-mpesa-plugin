<?php
/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/18/18
 * Time: 19:57
 */


function logger($error)
{
    $mpesa = new FanisiPayments();
    return $mpesa->logger($error);


}

function stkPush($planId, $phonenumber, $accountRefference, $transactionDesc)
{
    $mpesa = new FanisiPayments();
    return $mpesa->stkPush($planId, $phonenumber, $accountRefference, $transactionDesc);
}

function stkResponseHandler($response)
{
    $mpesa = new FanisiPayments();
    $amount = $reciept = '';
    $response = json_decode($response);
    $message = $response->Body->stkCallback->ResultDesc;
    if ($response->Body->stkCallback->ResultCode == 0) {
        $CheckoutRequestID = $response->Body->stkCallback->CheckoutRequestID;
        $CallbackMetadata = $response->Body->stkCallback->CallbackMetadata;
        $item = $CallbackMetadata->Item;

        foreach ($item as $mp) {
            if ($mp->Name == "Amount") {
                $amount = $mp->Value;
            }
            if ($mp->Name == 'MpesaReceiptNumber') {
                $reciept = $mp->Value;
            }
        }

    }
    $mpesa->stkUpdateTransaction($CheckoutRequestID, $reciept, $message);
    return true;


}

function getSubscriptions()
{

    global $wpdb;
    $plans_table = $wpdb->prefix . 'fanisi_subscription_plans';
    $users_table = $wpdb->prefix . 'users';
    $users_table_join = $users_table . '.ID';
    $plans_table_join = $plans_table . '.user_id';
    $sql = "SELECT *  FROM {$plans_table} 
                INNER JOIN {$users_table} ON {$plans_table_join} =  {$users_table_join} ";
    return $result = $wpdb->get_results($sql, OBJECT);
}

function register_url()
{
    $mpesa = new FanisiPayments();
    return $mpesa->registerUrl();
}

function c2bSimulate($planId, $phoneNumber, $reference)
{
    $mpesa = new FanisiPayments();
    return $mpesa->c2bSimulate($planId, $phoneNumber, $reference);
}

function c2bTransactionStatus($phoneNumber, $transactionId)
{
    $mpesa = new FanisiPayments();
    return $mpesa->c2bTransactionStatus($phoneNumber, $transactionId);
}

function c2bResponseHandler($response)
{
    $mpesa = new FanisiPayments();
    $response = json_decode($response);
    return $mpesa->c2bUpdateTransaction($response);

}

