<?php
/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/18/18
 * Time: 17:12
 */


function generate_subscription_button($atts)
{
    $options = (object)get_option('fanisi_payment_option_name');
    $params = shortcode_atts(array(
        'subscription' => '',
        'class' => 'button fs_button',
        'title' => 'Subscribe',
    ), $atts);

    $button = '<a href="' . $options->fanisi_payments_mpesa_processing_page .
        '?sb=' . urlencode(base64_encode($params['subscription'])) .
        '" class="' . $params['class'] . '" >' . str_replace('-', ' ', $params['title']) . '</a>';

    return $button;
}

add_shortcode('fanisi', 'generate_subscription_button');


