<?php
/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/17/18
 * Time: 11:52
 */
require FANISI_PAYMENTS_PLUGIN_DIR . 'vendor/autoload.php';
use Fanishi\Payments\Mpesa\Mpesa;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class FanisiPayments
{

    protected $mpesa;

    function __construct()
    {
        $creds = (object)get_option('fanisi_payment_option_name');

        $params = array(
            'consumerKey' => $creds->fanisi_payments_mpesa_consumer_key,
            'consumerSecret' => $creds->fanisi_payments_mpesa_consumer_secret,
            'payBill' => $creds->fanisi_payments_mpesa_paybill,
            'callbackUrl' => $creds->fanisi_payments_mpesa_callback_url,
            'timeoutUrl' => $creds->fanisi_payments_mpesa_timeout_url,
            'passKey' => $creds->fanisi_payments_mpesa_passkey,
            'c2bConfirmationUrl' => $creds->fanisi_payments_mpesa_c2b_confirmation_url,
            'c2bValidationUrl' => $creds->fanisi_payments_mpesa_c2b_validation_url,
            'c2bResultUrl' => $creds->fanisi_payments_mpesa_c2b_result_url,
            'initiatorName' => $creds->fanisi_payments_mpesa_initiator_name,
            'securityCredential' => $creds->fanisi_payments_mpesa_security_credential
        );
        $isSandbox = $creds->fanisi_payments_mpesa_sandbox ? TRUE : FALSE;
        $this->mpesa = new Mpesa($params, $isSandbox);


    }

    function newMpesa()
    {
        return $this->mpesa;
    }

    function logger($error)
    {
        $log = new Logger('fanisi_mpesa');
        $log->pushHandler(new StreamHandler(FANISI_PAYMENTS_PLUGIN_DIR . 'logs/mpesa.log', Logger::ERROR));

        if (is_array($error)) {
            $log->error('Mpesa', $error);
        }
        $log->error($error);

    }

    function getPlan($planId)
    {
        global $wpdb;
        $plans_table = $wpdb->prefix . 'fanisi_subscription_plans';
        $query = 'SELECT * FROM ' . $plans_table;
        $plan = $wpdb->get_results($query, OBJECT);
        if (key_exists(0, $plan)) {
            if (property_exists($plan[0], 'id') && property_exists($plan[0], 'plan_amount')) {
                if (!empty($plan[0]->plan_amount) && is_numeric($plan[0]->plan_amount)) {
                    return
                        array('error' => false,
                            'amount' => round($plan[0]->plan_amount));
                }
                return array(
                    'error' => true,
                    'msg' => 'Invalid Plan Amount'
                );
            }
            return array(
                'error' => true,
                'msg' => 'Invalid Plan'
            );

        }
        return array(
            'error' => true,
            'msg' => 'Plan does not exist'
        );

    }


    function stkPush($planId, $phonenumber, $accountRefference, $transactionDesc)
    {
        global $wpdb;
        $payments_table = $wpdb->prefix . 'fanisi_payments';
        $plan = $this->getPlan($planId);
        if (!$plan['error']) {
            $amount = $plan['amount'];
        }
        $response = $this->mpesa->stkPush($amount, $phonenumber, $accountRefference, $transactionDesc);
        if (property_exists($response, 'errorCode')) {
            $error = (array)$response;
            $this->logger($error);

            return array(
                'error' => true,
                'msg' => 'mpesa error, if you are the admin please check the mpesa logs'
            );
        }

        if (is_user_logged_in()) {
            $user = wp_get_current_user();
            $wpdb->insert($payments_table, array(
                'time' => date('Y-m-d h:m:s'),
                'user_id' => $user->ID,
                'merchant_request_id' => $response->MerchantRequestID,
                'checkout_request_id' => $response->CheckoutRequestID,
                'response_code' => $response->ResponseCode,
                'amount' => $amount,
                'phone_number' => $phonenumber,
                'plan_id' => $planId,
                'description' => $transactionDesc,
                'status' => 'PENDING',
                'isC2B' => 0,
            ));

            return array(
                'error' => false,
                'msg' => $response->CustomerMessage
            );
        }

        return array(
            'error' => true,
            'msg' => 'user is not logged in. Please login to continue'
        );


    }

    function stkUpdateTransaction($CheckoutRequestID, $reciept)
    {
        global $wpdb;
        $payments_table = $wpdb->prefix . 'fanisi_payments';
        $wpdb->update(
            $payments_table,
            array(
                'status' => 'PAID',
                'mpesa_reciept' => $reciept
            ),
            array('checkout_request_id' => $CheckoutRequestID),
            array(
                '%s',
                '%s'
            ),
            array('%s')
        );
        $this->createSubscriber();
        return true;

    }

    function registerUrl()
    {
        return $this->mpesa->registerUrl();
    }

    function c2bSimulate($planId, $phoneNumber, $reference)
    {
        global $wpdb;
        $payments_table = $wpdb->prefix . 'fanisi_payments';
        $plan = $this->getPlan($planId);
        if (!$plan['error']) {
            $amount = $plan['amount'];
        }
        $response = $this->mpesa->c2bSimulate($amount, $phoneNumber, $reference);


        if (is_user_logged_in()) {
            $user = wp_get_current_user();
            $wpdb->insert($payments_table, array(
                'time' => date('Y-m-d h:m:s'),
                'user_id' => $user->ID,
                'merchant_request_id' => NULL,
                'checkout_request_id' => NULL,
                'response_code' => NULL,
                'amount' => $amount,
                'phone_number' => $phoneNumber,
                'plan_id' => $planId,
                'description' => $reference,
                'status' => 'PENDING',
                'isC2B' => 1,
                'conversation_id' => $response->ConversationID,
                'originator_coversation_id' => $response->OriginatorCoversationID
            ));

            return array(
                'error' => false,
                'msg' => $response->ResponseDescription
            );
        }
        return array(
            'error' => true,
            'msg' => 'user is not logged in. Please login to continue'
        );

    }

    function c2bUpdateTransaction($response)
    {
        $str = '            { "TransactionType": "Pay Bill",             "TransID": "MES71H545P",             "TransTime": "20180528174811",             "TransAmount": "1.00",             "BusinessShortCode": "600405",             "BillRefNumber": "v01",             "InvoiceNumber": "",             "OrgAccountBalance": "202101.00",             "ThirdPartyTransID": "",             "MSISDN": "254708374149",             "FirstName": "John",             "MiddleName": "J.",             "LastName": "Doe"         } ';
        $str = json_decode($str);
        var_dump($str);
        exit;
        // var_dump($response);
        $result = $response->Result;
        if ($result->ResultCode == 0) {

            global $wpdb;
            $payments_table = $wpdb->prefix . 'fanisi_payments';
            $wpdb->update(
                $payments_table,
                array(
                    'status' => 'PAID',
                    'mpesa_reciept' => $response->TransID,
                ),
                array(
                    'description' => $response->BillRefNumber
                ),
                array(
                    '%s',
                ),
                array('%s')
            );
            $this->createSubscriber();
            return array(
                'error' => false,
                'msg' => $result->ResultDesc,
            );
        }

        return array(
            'error' => true,
            'msg' => $result->ResultDesc,
        );


    }

    function c2bTransactionStatus($phoneNumber, $transactionId)
    {
        return $this->mpesa->c2bTransactionStatus($phoneNumber, $transactionId);
    }


    function getUserSubscriptions($userId)
    {

    }

    function createSubscriber()
    {
        global $wpdb;
        $payments_table = $wpdb->prefix . 'fanisi_subscribers';
        $user = wp_get_current_user();
        $wpdb->insert($payments_table, array(
            'time' => date('Y-m-d h:m:s'),
            'user_id' => $user->ID,
            ' isSubscription_active' => 1,
            'subscription_start' => NULL,
            'subscription_end' => NULL,
        ));
    }

}