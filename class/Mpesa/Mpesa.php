<?php


namespace Fanishi\Payments\Mpesa;
require FANISI_PAYMENTS_PLUGIN_DIR . 'vendor/autoload.php';

use GuzzleHttp;

/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/17/18
 * Time: 12:57
 */
class Mpesa
{
    protected $consumerkey;
    protected $consumerSecret;
    protected $callback;
    protected $client;
    protected $baseUrl;
    protected $paybill;
    protected $passkey;
    protected $confirmationUrl;
    protected $validationUrl;
    protected $resultUrl;
    protected $securityCredential;
    protected $isSandbox;


    function __construct(array $setup, $isSandbox)
    {
        $this->isSandbox = $isSandbox;
        $this->consumerkey = $setup['consumerKey'];
        $this->consumerSecret = $setup['consumerSecret'];
        $this->callback = $setup['callbackUrl'];
        $this->paybill = $setup['payBill'];
        $this->timeOutUrl = $setup['timeoutUrl'];
        $this->passkey = $setup['passKey'];
        $this->confirmationUrl = $setup['c2bConfirmationUrl'];
        $this->validationUrl = $setup['c2bValidationUrl'];
        $this->resultUrl = $setup['c2bResultUrl'];
        $this->securityCredential = str_replace(' ', '', $setup['securityCredential']);
        $this->initatorName = $setup['initiatorName'];
        $this->client = new  GuzzleHttp\Client();

        $this->baseUrl = 'https://api.safaricom.co.ke/';
        if ($this->isSandbox) {
            $this->baseUrl = 'https://sandbox.safaricom.co.ke/';
        }
    }

    public function generateToken()
    {
        $keySecret = $this->consumerkey . ':' . $this->consumerSecret;
        $credentials = base64_encode($keySecret);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $this->baseUrl . 'oauth/v1/generate?grant_type=client_credentials');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responce = json_decode($curl_response);
        return $responce->access_token;

    }

    function stkPush($amount, $phonenumber, $accountRefference, $transactionDesc)
    {
        $url = $this->baseUrl . 'mpesa/stkpush/v1/processrequest';
        $t = time();
        $timestamp = '20' . date("ymdhis");
        $conct = $this->paybill . $this->passkey . $timestamp;
        $password = base64_encode($conct);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => $this->paybill,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'PartyA' => $phonenumber,
            'PartyB' => $this->paybill,
            'PhoneNumber' => $phonenumber,
            'CallBackURL' => $this->callback,
            'AccountReference' => $accountRefference,
            'TransactionDesc' => $transactionDesc
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);
        return json_decode($curl_response);

    }

    function stkQuery($checkoutRequestId)
    {
        $url = $this->baseUrl . '/mpesa/stkpushquery/v1/query';
        $timestamp = '20' . date("ymdhis");
        $conct = $this->paybill . $this->passkey . $timestamp;
        $password = base64_encode($conct);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => $this->paybill,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'CheckoutRequestID' => $checkoutRequestId
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        return json_decode($curl_response);
    }

    function registerUrl()
    {

        $url = $this->baseUrl . '/mpesa/c2b/v1/registerurl';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'ShortCode' => $this->isSandbox ? '600405' : $this->paybill,
            'ResponseType' => 'completed',
            'ConfirmationURL' => $this->confirmationUrl,
            'ValidationURL' => $this->validationUrl,
        );

        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        return json_decode($curl_response);

    }

    function c2bSimulate($amount, $phoneNumber, $reference = '0000')
    {
        $url = $this->baseUrl . '/mpesa/c2b/v1/simulate';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header


        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'ShortCode' => $this->isSandbox ? '600405' : $this->paybill,
            'CommandID' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'Msisdn' => $phoneNumber,
            'BillRefNumber' => $reference
        );

        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        return json_decode($curl_response);

    }


    function c2bAccountBalance($phoneNumber, $remarks = '')
    {
        $url = $this->baseUrl . '/mpesa/accountbalance/v1/query';

        $timestamp = '20' . date("ymdhis");
        $conct = $this->paybill . $this->passkey . $timestamp;
        $securityCredential = base64_encode($this->securityCredential);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'Initiator' => $this->initatorName,
            'SecurityCredential' => $securityCredential,
            'CommandID' => 'AccountBalance',
            'PartyA' => $phoneNumber,
            'IdentifierType' => '4',
            'Remarks' => $remarks,
            'QueueTimeOutURL' => $this->timeOutUrl,
            'ResultURL' => $this->callback
        );
        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);

        return json_decode($curl_response);
    }

    function c2bTransactionStatus($phoneNumber, $transactionId)
    {

        $url = $this->baseUrl . '/mpesa/transactionstatus/v1/query';
        $timestamp = '20' . date("ymdhis");
        $conct = $this->paybill . $this->passkey . $timestamp;
        $securityCredential = base64_encode($this->securityCredential);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $this->generateToken())); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'Initiator' => $this->initatorName,
            'SecurityCredential' => $securityCredential,
            'CommandID' => 'TransactionStatusQuery',
            'TransactionID' => $transactionId,
            'PartyA' => $phoneNumber,
            'IdentifierType' => '1',
            'ResultURL' => $this->resultUrl,
            'QueueTimeOutURL' => $this->timeOutUrl,
            'Remarks' => 'Transaction Online Query',
            'Occasion' => 'System Validation'
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        return json_decode($curl_response);

    }

    function reverse($amount, $recievingPhoneNumber, $transactionId, $remarks = ' ', $occasion = ' ')
    {
        $url = $this->baseUrl . '/mpesa/reversal/v1/request';
        $timestamp = '20' . date("ymdhis");
        $conct = $this->paybill . $this->passkey . $timestamp;
        $securityCredential = base64_encode($this->securityCredential);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ACCESS_TOKEN')); //setting custom header
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'Initiator' => $this->initatorName,
            'SecurityCredential' => $securityCredential,
            'CommandID' => 'TransactionReversal',
            'TransactionID' => $transactionId,
            'Amount' => $amount,
            'ReceiverParty' => $recievingPhoneNumber,
            'RecieverIdentifierType' => '4',
            'ResultURL' => $this->callback,
            'QueueTimeOutURL' => $this->timeOutUrl,
            'Remarks' => $remarks,
            'Occasion' => $occasion
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        return json_decode($curl_response);
    }

}

