<?php

/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/17/18
 * Time: 11:58
 */
use fanisi\view\admin;

class FanisiAdmin
{

    protected $adminView;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin',
            'Fanisi Settings',
            'manage_options',
            'fanisi-setting-admin',
            array($this, 'create_admin_page')
        );

        add_options_page(
            'Settings Admin',
            'Fanisi Subscription Plans',
            'manage_options',
            'fanisi-subscription-plan-setting-admin',
            array($this, 'create_admin_subscription_plan_page')
        );
        add_options_page(
            'Settings Admin',
            'Fanisi Payments',
            'manage_options',
            'fanisi-payment-setting-admin',
            array($this, 'create_admin_fanisi_payments')
        );

    }


    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('fanisi_payment_option_name');
        ?>
        <div class="wrap">
            <h1>Fanisi Payments Settings</h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('fanisi_payment_option_group');
                do_settings_sections('fanisi-payments-setting-admin');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'fanisi_payment_option_group', // Option group
            'fanisi_payment_option_name', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'mpesa_setting_section_id', // ID
            'Mpesa Settings', // Title
            array($this, 'print_section_info'), // Callback
            'fanisi-payments-setting-admin' // Page
        );


        add_settings_field(
            'fanisi_payments_mpesa_paybill',
            'Mpesa Paybil/Till Number',
            array($this, 'fanisi_payments_mpesa_paybill_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_consumer_key', // ID
            'Mpesa Consumer Key', // Title
            array($this, 'fanisi_payments_mpesa_consumer_key_callback'), // Callback
            'fanisi-payments-setting-admin', // Page
            'mpesa_setting_section_id' // Section
        );

        add_settings_field(
            'fanisi_payments_mpesa_consumer_secret',
            'Mpesa Consumer Secret',
            array($this, 'fanisi_payments_mpesa_consumer_secret_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_passkey',
            'Mpesa Pass Key',
            array($this, 'fanisi_payments_mpesa_passkey_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_callback_url',
            'Mpesa STK Callback Url',
            array($this, 'fanisi_payments_mpesa_callback_url_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );


        add_settings_field(
            'fanisi_payments_mpesa_processing_page',
            'Mpesa STK  Listener Page ',
            array($this, 'fanisi_payments_mpesa_processing_page_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_c2b_validation_url',
            'Mpesa C2B Validation ',
            array($this, 'fanisi_payments_mpesa_c2b_validation_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_c2b_confirmation_url',
            'Mpesa C2B  Confirmation Url  ',
            array($this, 'fanisi_payments_mpesa_c2b_confirmation_url_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_c2b_result_url',
            'Mpesa C2B  Listener ',
            array($this, 'fanisi_payments_mpesa_c2b_listener_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_timeout_url',
            'Timeout Url ',
            array($this, 'fanisi_payments_mpesa_timeout_url_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_inititor_name',
            'Initiator Name ',
            array($this, 'fanisi_payments_mpesa_initiator_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );

        add_settings_field(
            'fanisi_payments_mpesa_security_credential',
            'Security Credential',
            array($this, 'fanisi_payments_mpesa_security_credential_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );


        add_settings_field(
            'fanisi_payments_mpesa_sandbox',
            'Mode',
            array($this, 'fanisi_payments_mpesa_sandbox_callback'),
            'fanisi-payments-setting-admin',
            'mpesa_setting_section_id'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['fanisi_payments_mpesa_consumer_key'])) {
            $new_input['fanisi_payments_mpesa_consumer_key'] = sanitize_text_field($input['fanisi_payments_mpesa_consumer_key']);
        }


        if (isset($input['fanisi_payments_mpesa_consumer_secret'])) {
            $new_input['fanisi_payments_mpesa_consumer_secret'] = sanitize_text_field($input['fanisi_payments_mpesa_consumer_secret']);
        }

        if (isset($input['fanisi_payments_mpesa_callback_url'])) {
            $new_input['fanisi_payments_mpesa_callback_url'] = sanitize_url($input['fanisi_payments_mpesa_callback_url']);
        }
        if (isset($input['fanisi_payments_mpesa_timeout_url'])) {
            $new_input['fanisi_payments_mpesa_timeout_url'] = sanitize_url($input['fanisi_payments_mpesa_timeout_url']);
        }

        if (isset($input['fanisi_payments_mpesa_paybill'])) {
            $new_input['fanisi_payments_mpesa_paybill'] = sanitize_text_field($input['fanisi_payments_mpesa_paybill']);
        }

        if (isset($input['fanisi_payments_mpesa_passkey'])) {
            $new_input['fanisi_payments_mpesa_passkey'] = sanitize_text_field($input['fanisi_payments_mpesa_passkey']);
        }

        if (isset($input['fanisi_payments_mpesa_processing_page'])) {
            $new_input['fanisi_payments_mpesa_processing_page'] = sanitize_text_field($input['fanisi_payments_mpesa_processing_page']);
        }

        if (isset($input['fanisi_payments_mpesa_sandbox'])) {
            $new_input['fanisi_payments_mpesa_sandbox'] = sanitize_text_field($input['fanisi_payments_mpesa_sandbox']);
        }

        if (isset($input['fanisi_payments_mpesa_c2b_result_url'])) {
            $new_input['fanisi_payments_mpesa_c2b_result_url'] = sanitize_text_field($input['fanisi_payments_mpesa_c2b_result_url']);
        }

        if (isset($input['fanisi_payments_mpesa_c2b_confirmation_url'])) {
            $new_input['fanisi_payments_mpesa_c2b_confirmation_url'] = sanitize_text_field($input['fanisi_payments_mpesa_c2b_confirmation_url']);
        }
        if (isset($input['fanisi_payments_mpesa_c2b_validation_url'])) {
            $new_input['fanisi_payments_mpesa_c2b_validation_url'] = sanitize_text_field($input['fanisi_payments_mpesa_c2b_validation_url']);
        }

        if (isset($input['fanisi_payments_mpesa_initiator_name'])) {
            $new_input['fanisi_payments_mpesa_initiator_name'] = sanitize_text_field($input['fanisi_payments_mpesa_initiator_name']);
        }
        if (isset($input['fanisi_payments_mpesa_security_credential'])) {
            $new_input['fanisi_payments_mpesa_security_credential'] = sanitize_text_field($input['fanisi_payments_mpesa_security_credential']);
        }


        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_consumer_key_callback()
    {
        printf(
            '<input type="password" id="fanisi_payments_mpesa_consumer_key" name="fanisi_payment_option_name[fanisi_payments_mpesa_consumer_key]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_consumer_key']) ? esc_attr($this->options['fanisi_payments_mpesa_consumer_key']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_consumer_secret_callback()
    {
        printf(
            '<input type="password" id="fanisi_payments_mpesa_consumer_secret" name="fanisi_payment_option_name[fanisi_payments_mpesa_consumer_secret]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_consumer_secret']) ? esc_attr($this->options['fanisi_payments_mpesa_consumer_secret']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_callback_url_callback()
    {
        printf(
            '<input type="url" id="fanisi_payments_mpesa_callback_url" name="fanisi_payment_option_name[fanisi_payments_mpesa_callback_url]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_callback_url']) ? esc_attr($this->options['fanisi_payments_mpesa_callback_url']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_timeout_url_callback()
    {
        printf(
            '<input type="url" id="fanisi_payments_mpesa_timeout_url" name="fanisi_payment_option_name[fanisi_payments_mpesa_timeout_url]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_timeout_url']) ? esc_attr($this->options['fanisi_payments_mpesa_timeout_url']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_paybill_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_paybill" name="fanisi_payment_option_name[fanisi_payments_mpesa_paybill]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_paybill']) ? esc_attr($this->options['fanisi_payments_mpesa_paybill']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_passkey_callback()
    {
        printf(
            '<input type="password" id="fanisi_payments_mpesa_passkey" name="fanisi_payment_option_name[fanisi_payments_mpesa_passkey]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_passkey']) ? esc_attr($this->options['fanisi_payments_mpesa_passkey']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_processing_page_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_processing_page" name="fanisi_payment_option_name[fanisi_payments_mpesa_processing_page]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_processing_page']) ? esc_attr($this->options['fanisi_payments_mpesa_processing_page']) : ''
        );
    }


    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_c2b_validation_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_c2b_validation_url" name="fanisi_payment_option_name[fanisi_payments_mpesa_c2b_validation_url]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_c2b_validation_url']) ? esc_attr($this->options['fanisi_payments_mpesa_c2b_validation_url']) : ''
        );
    }

    public function fanisi_payments_mpesa_c2b_confirmation_url_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_c2b_confirmation_url" name="fanisi_payment_option_name[fanisi_payments_mpesa_c2b_confirmation_url]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_c2b_confirmation_url']) ? esc_attr($this->options['fanisi_payments_mpesa_c2b_confirmation_url']) : ''
        );
    }

    public function fanisi_payments_mpesa_c2b_listener_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_c2b_result_url" name="fanisi_payment_option_name[fanisi_payments_mpesa_c2b_result_url]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_c2b_result_url']) ? esc_attr($this->options['fanisi_payments_mpesa_c2b_result_url']) : ''
        );
    }

    public function fanisi_payments_mpesa_initiator_callback()
    {
        printf(
            '<input type="text" id="fanisi_payments_mpesa_initiator_name" name="fanisi_payment_option_name[fanisi_payments_mpesa_initiator_name]" value="%s" />',
            isset($this->options['fanisi_payments_mpesa_initiator_name']) ? esc_attr($this->options['fanisi_payments_mpesa_initiator_name']) : ''
        );
    }

    public function fanisi_payments_mpesa_security_credential_callback()
    {
        printf(
            '<textarea cols="80" rows="8"  id="fanisi_payments_mpesa_security_credential" name="fanisi_payment_option_name[fanisi_payments_mpesa_security_credential]"  />%s</textarea>',
            isset($this->options['fanisi_payments_mpesa_security_credential']) ? esc_attr($this->options['fanisi_payments_mpesa_security_credential']) : ''
        );
    }


    /**
     * Get the settings option array and print one of its values
     */
    public function fanisi_payments_mpesa_sandbox_callback()
    {
        ?>

        <input type="radio" name="fanisi_payment_option_name[fanisi_payments_mpesa_sandbox]"
               value="1" <?php checked(1, $this->options['fanisi_payments_mpesa_sandbox'], true); ?>> Sandbox
        <input type="radio" name="fanisi_payment_option_name[fanisi_payments_mpesa_sandbox]"
               value="0" <?php checked(0, $this->options['fanisi_payments_mpesa_sandbox'], true); ?>> Production

        <?php
    }


    function getMpesaOptions()
    {
        return get_option('fanisi_payment_option_name');

    }

    public function create_admin_fanisi_payments()
    {
        $payments = $this->getPayments()
        ?>
        <div class="wrap">
            <h1>Fanisi Payments</h1>
            <table id="fanisi-table" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>Subscription Plan</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Reciept</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($payments as $result): ?>
                    <tr>
                        <td><?php echo $result->plan_name ?></td>
                        <td><?php echo $result->user_nicename ?></td>
                        <td><?php echo $result->phone_number ?></td>
                        <td><?php echo $result->amount ?></td>
                        <td><?php echo $result->status ?></td>
                        <td><?php echo $result->mpesa_reciept ?></td>
                        <td><?php echo $result->time ?></td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Subscription Plan</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Reciept</th>
                    <th>Date</th>
                </tr>
                </tfoot>
            </table>

        </div>
        <?php
    }

    function getPayments()
    {
        global $wpdb;
        $payments_table = $wpdb->prefix . 'fanisi_payments';
        $plans_table = $wpdb->prefix . 'fanisi_subscription_plans';
        $users_table = $wpdb->prefix . 'users';
        $payments_table_join = $payments_table . '.user_id';
        $users_table_join = $users_table . '.ID';
        $payments_table_join_plan = $payments_table . '.plan_id';
        $plans_table_join = $plans_table . '.id';

        $sql = "SELECT *  FROM {$payments_table} 
                INNER JOIN {$users_table} ON {$payments_table_join} =  {$users_table_join} 
                INNER JOIN {$plans_table } ON {$payments_table_join_plan} =  {$plans_table_join}";
        $result = $wpdb->get_results($sql, OBJECT);
        return $result;
    }

    function getSubscribers()
    {

    }

    /**
     *  Subscription page callback
     */
    public function create_admin_subscription_plan_page()
    {
        $plans = $this->getPlans()
        ?>
        <div class="wrap">
            <h1>Fanisi subscription plans</h1>
            <table id="fanisi-table" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>Plan Id</th>
                    <th> Plan Name</th>
                    <th>Plan Description</th>
                    <th>Plan Amount</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Creator Email</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($plans as $result): ?>
                    <tr>
                        <td><?php echo $result->id ?></td>
                        <td><?php echo $result->plan_name ?></td>
                        <td><?php echo $result->plan_description ?></td>
                        <td><?php echo $result->plan_amount ?></td>
                        <td><?php echo $result->time ?></td>
                        <td><?php echo $result->user_nicename ?></td>
                        <td><?php echo $result->user_email ?></td>
                        <td>
                            <a class="fanisi-edit" data-fanisi-edit-id="fanisi-edit-<?php echo $result->id ?>">
                                Edit</a>
                            <div class="modal" id="fanisi-edit-<?php echo $result->id ?>">
                                <!-- data-iziModal-fullscreen="true"  data-iziModal-title="Welcome"  data-iziModal-subtitle="Subtitle"  data-iziModal-icon="icon-home" -->
                                <div>
                                    <form class="fanisi-form">
                                        <label>Plan Name</label><br/>
                                        <input type="text" class="" value="<?php echo $result->plan_name ?>">
                                        <br/>
                                        <label>Plan Amount</label>
                                        <br/>
                                        <input type="number" class="" value="<?php echo $result->plan_amount ?>">
                                        <br/>
                                        <label>Plan Description</label><br/>
                                        <textarea><?php echo $result->plan_description ?></textarea>
                                        <br/>
                                        <button class="button-primary"> Save</button>
                                    </form>
                                </div>
                            </div>
                        </td>


                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Plan Id</th>
                    <th> Plan Name</th>
                    <th>Plan Description</th>
                    <th>Plan Amount</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Creator Email</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <?php
    }


    function getPlans()
    {

        global $wpdb;
        $plans_table = $wpdb->prefix . 'fanisi_subscription_plans';
        $users_table = $wpdb->prefix . 'users';
        $users_table_join = $users_table . '.ID';
        $plans_table_join = $plans_table . '.user_id';
        $sql = "SELECT *  FROM {$plans_table} 
                INNER JOIN {$users_table} ON {$plans_table_join} =  {$users_table_join} ";
        return $result = $wpdb->get_results($sql, OBJECT);
    }

}