<?php
/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/17/18
 * Time: 11:36
 * Plugin Name: Fanisi Mpesa Payments
 * Plugin URI: https://tinker.co.ke
 * Description: A plugin  that enables   Fanisi Academies to leverage the power of mpesa payments
 * Version: 0.0.1
 * Author: Tinker Digital
 * Author URI: https://tinker.co.ke
 * License: GPLv2 or later
 * Text Domain: Tinker
 */

define('FANISI_PAYMENTS_PLUGIN_DIR', plugin_dir_path(__FILE__));
// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

register_activation_hook(__FILE__, 'create_plugin_database_table');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'class/FanisiAdmin.php');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'class/Mpesa/Mpesa.php');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'class/pageTemplater.php');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'class/FanisiPayments.php');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'shortcodes.php');
require_once(FANISI_PAYMENTS_PLUGIN_DIR . 'functions.php');


if (is_admin() || (defined('WP_CLI') && WP_CLI)) {
    $settings = new  FanisiAdmin();
}

add_action('plugins_loaded', array('PageTemplater', 'get_instance'));

function create_plugin_database_table()
{

    global $wpdb, $wnm_db_version;
    $payments_table = $wpdb->prefix . 'fanisi_payments';
    $subscribers_table = $wpdb->prefix . 'fanisi_subscribers';
    $plans_table = $wpdb->prefix . 'fanisi_subscription_plans';
    $sql = array();

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE  $payments_table (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            user_id mediumint(9) NOT NULL,
            merchant_request_id varchar(255)  NULL,
            checkout_request_id varchar(255)  NULL,
            originator_coversation_id varchar(255)  NULL,
            conversation_id varchar(255)  NULL,
            response_code varchar(255)  NULL,
            isC2B tinyint(1) NOT NULL,
            amount varchar(255) NOT NULL,
            phone_number varchar(255) NOT NULL,
            plan_id varchar(255) NOT NULL,
            description varchar(255) NOT NULL,
            status varchar(255) NOT NULL,
            mpesa_reciept varchar(255),
            
          
            
            PRIMARY KEY  (id)
            ) $charset_collate;";


    $sql_1 = "CREATE TABLE  $subscribers_table (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            user_id mediumint(9) NOT NULL,
            isSubscription_active tinyint NOT NULL,
            subscription_start datetime NOT NULL,
            subscription_end datetime NOT NULL,
          
            PRIMARY KEY  (id)
            ) $charset_collate;";

    $sql_2 = "CREATE TABLE  $plans_table (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            user_id mediumint(9) NOT NULL,
            plan_name TEXT NOT NULL,
            plan_description TEXT NOT NULL,
            plan_amount decimal(10,2) NOT NULL,
            plan_duration mediumint(9) NOT NULL,
          
            PRIMARY KEY  (id)
            ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    dbDelta($sql_1);
    dbDelta($sql_2);

}

function load_custom_wp_admin_style($hook)
{
    if ($hook == 'settings_page_fanisi-payment-setting-admin' || $hook == 'settings_page_fanisi-subscribers-setting-admin' || $hook == 'settings_page_fanisi-subscription-plan-setting-admin') {
        wp_enqueue_script('fanisi-datatable-jquery-script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js');
        wp_enqueue_style('fanisi-datatable-css', 'https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css');
        wp_enqueue_script('fanisi-datatable-script', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js');

        wp_enqueue_style('fanisi-datatable-css-izzi', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css');
        wp_enqueue_script('fanisi-datatable-script-izzi', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js');

        wp_enqueue_style('fanisi_css', plugins_url('assets/css/fanisi.css', __FILE__));
        wp_enqueue_script('fanisi_stript', plugins_url('assets/js/fanisi.js', __FILE__));


    }

}

add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');





