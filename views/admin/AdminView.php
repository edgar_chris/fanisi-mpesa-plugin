<?php
/**
 * Created by PhpStorm.
 * User: edgarchris
 * Date: 5/17/18
 * Time: 12:11
 */

namespace fanisi\view\admin;


class AdminView
{

    public function adminSettingsPage()
    {
        ?>
        <div class="wrap">
            <h1>Fanisi Payment Options</h1>

            <form method="post" action="options.php">
                <?php settings_fields('fanisi-payment-settings-group'); ?>
                <?php do_settings_sections('fanisi-payment-settings-group'); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">New Option Name</th>
                        <td><input type="text" name="new_option_name"
                                   value="<?php echo esc_attr(get_option('new_option_name')); ?>"/></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Some Other Option</th>
                        <td><input type="text" name="some_other_option"
                                   value="<?php echo esc_attr(get_option('some_other_option')); ?>"/></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Options, Etc.</th>
                        <td><input type="text" name="option_etc"
                                   value="<?php echo esc_attr(get_option('option_etc')); ?>"/></td>
                    </tr>
                </table>

                <?php submit_button(); ?>

            </form>
        </div>

    <?php }
}